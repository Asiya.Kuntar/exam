LINE=$(cat "$1")
CHAR="$2"
TOTAL=$(echo "${#LINE}")
CHAR_NUMBER=$(echo -e "$LINE" | grep -o "$CHAR" | wc -l)
RESULT=$(echo "$CHAR_NUMBER/$TOTAL" | bc -l)
echo $RESULT
